**Description**
----------------
Microservice c# template using the CLEAN architecture and CQRS pattern.

Version .NET 7.0 

**Installation and usage**
----------------
**Install the template:**
1. Clone this repository.
2. Using your favourite CLI run `dotnet new -i .\microtemplate`
> if it was succesfully installed then you can see it in the template list under 'microtemplate' short name by running `dotnet new -l`

**Use the template:**
1. Create a folder in which you want to add the new project.
2. Run `dotnet new microtemplate --name "MicroserviceNameGoesHere"`
> the `name` parameter will replace `MService` in the entire project structure. Set it something like `User` for User.Api. (you might need to add --force depending on the CLI used)

In case something went wrong reset to the default templates then try again.
1. `dotnet new --debug:reinit`

References:
- https://jasontaylor.dev/clean-architecture-getting-started/
- https://docs.microsoft.com/en-us/dotnet/core/tools/custom-templates
- https://devblogs.microsoft.com/dotnet/how-to-create-your-own-templates-for-dotnet-new/
