﻿namespace MService.Infrastructure.Services
{
    using System;
    using System.Threading.Tasks;
    using Application.Interfaces;

    public class GuidGenerator : IGuidGenerator
    {
        public async Task<Guid> GetNextAsync()
        {
            return await Task.FromResult(Guid.NewGuid());
        }
    }
}