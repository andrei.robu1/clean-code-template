﻿namespace MService.Infrastructure
{
    using Application.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Services;

    public static class InfrastructureDependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddScoped<IGuidGenerator, GuidGenerator>();

            return services;
        }
    }
}