﻿namespace MService.Application.Interfaces
{
    using System;
    using System.Threading.Tasks;

    public interface IGuidGenerator
    {
        Task<Guid> GetNextAsync();
    }
}