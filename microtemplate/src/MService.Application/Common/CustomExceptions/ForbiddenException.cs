﻿namespace MService.Application.Common.CustomExceptions
{
    using MService.Common.Resources;

    public class ForbiddenException : BaseException
    {
        public ForbiddenException()
        {
            UiMessage = ExceptionMessages.Forbidden;
        }

        public ForbiddenException(string resourceName)
        {
            UiMessage = $"{resourceName} is forbidden.";
        }
    }
}