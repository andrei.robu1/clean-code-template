﻿namespace MService.Application.Common.CustomExceptions
{
    using MService.Common.Resources;

    public class BadRequestException : BaseException
    {
        public BadRequestException()
        {
            UiMessage = ExceptionMessages.BadRequest;
        }

        public BadRequestException(string message)
            : base(message)
        {
            UiMessage = message;
        }
    }
}