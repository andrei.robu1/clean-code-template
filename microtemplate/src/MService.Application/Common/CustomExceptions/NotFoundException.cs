﻿namespace MService.Application.Common.CustomExceptions
{
    using MService.Common.Resources;

    public class NotFoundException : BaseException
    {
        public NotFoundException()
        {
            UiMessage = ExceptionMessages.NotFound;
        }

        public NotFoundException(string entity)
        {
            UiMessage = $"{entity} not found.";
        }
    }
}