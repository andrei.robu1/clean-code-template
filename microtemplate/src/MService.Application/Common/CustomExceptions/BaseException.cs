﻿namespace MService.Application.Common.CustomExceptions
{
    using System;

    public abstract class BaseException : Exception
    {
        /// <summary>
        ///     Base exception handler.
        /// </summary>
        protected BaseException()
        {
        }

        /// <summary>
        ///     Base exception handler.
        /// </summary>
        /// <param name="message">Exception message.</param>
        protected BaseException(string message)
            : base(message)
        {
        }

        public string UiMessage { get; protected set; }
    }
}