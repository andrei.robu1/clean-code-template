﻿namespace MService.Application.Users.Queries.GetUser
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Entities;
    using MediatR;

    public class UserQueryHandler : IRequestHandler<GetUserQuery, UserQueryVm>
    {
        public async Task<UserQueryVm> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            // get this from somewhere
            var user = new User
            {
                Id = request.UserId,
                Username = "Mark",
                Password = "password",
                About = "Likes C#."
            };

            var userVm = new UserQueryVm
            {
                Id = user.Id,
                Username = user.Username,
                About = user.About
            };

            return await Task.FromResult(userVm);
        }
    }
}