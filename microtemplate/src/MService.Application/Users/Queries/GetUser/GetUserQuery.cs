﻿namespace MService.Application.Users.Queries.GetUser
{
    using System;
    using MediatR;

    public class GetUserQuery : IRequest<UserQueryVm>
    {
        public Guid UserId { get; set; }
    }
}