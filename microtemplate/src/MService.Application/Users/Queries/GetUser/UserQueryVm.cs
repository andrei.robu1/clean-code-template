﻿namespace MService.Application.Users.Queries.GetUser
{
    using System;

    public class UserQueryVm
    {
        public Guid Id { get; set; }

        public string Username { get; set; }

        public string About { get; set; }
    }
}