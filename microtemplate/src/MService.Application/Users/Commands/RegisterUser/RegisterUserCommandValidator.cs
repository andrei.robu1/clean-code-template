﻿namespace MService.Application.Users.Commands.RegisterUser
{
    using FluentValidation;

    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator()
        {
            RuleFor(c => c.Username)
                .Must(BeUniqueUsername).WithMessage("Username already in use.");
        }

        private static bool BeUniqueUsername(string username)
        {
            return username != "Generic";
        }
    }
}