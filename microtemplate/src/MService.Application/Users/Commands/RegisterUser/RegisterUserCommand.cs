﻿namespace MService.Application.Users.Commands.RegisterUser
{
    using System;
    using MediatR;

    public class RegisterUserCommand : IRequest<Guid>
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string About { get; set; }
    }
}