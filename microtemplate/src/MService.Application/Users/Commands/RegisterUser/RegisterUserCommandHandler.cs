﻿namespace MService.Application.Users.Commands.RegisterUser
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Entities;
    using Interfaces;
    using MediatR;

    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, Guid>
    {
        private readonly IGuidGenerator _guidGenerator;

        public RegisterUserCommandHandler(IGuidGenerator guidGenerator)
        {
            _guidGenerator = guidGenerator;
        }

        public async Task<Guid> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var id = await _guidGenerator.GetNextAsync();

            var user = new User
            {
                Id = id,
                Username = request.Username,
                Password = request.Password,
                About = request.About
            };

            // do stuff like save the user to db by injecting a repository interface in this constructor
            return await Task.FromResult(user.Id);
        }
    }
}