﻿namespace MService.Persistence
{
    using Microsoft.Extensions.DependencyInjection;

    public static class PersistenceDependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            //inject persistence dependencies here
            return services;
        }
    }
}