﻿namespace MService.Api.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Application.Users.Commands.RegisterUser;
    using Application.Users.Queries.GetUser;
    using Microsoft.AspNetCore.Mvc;

    public class UsersController : ApiController
    {
        /// <summary>
        ///     This flow is only for exemplification, remove it.
        /// </summary>
        /// <param name="request">User details.</param>
        /// <returns>User guid.</returns>
        [HttpPost]
        public async Task<ActionResult> Register(RegisterUserCommand request)
        {
            var userId = await Mediator.Send(request);

            return Created(" ", userId);
        }

        /// <summary>
        ///     This flow is only for exemplification, remove it.
        /// </summary>
        /// <param name="userId">GUID user id. Try: 00000000-0000-0000-0000-000000000000 .</param>
        /// <returns>Hardcoded user details.</returns>
        [HttpGet("user")]
        public async Task<ActionResult> UserDetails(Guid userId)
        {
            var userVm = await Mediator.Send(new GetUserQuery { UserId = userId });

            return Ok(userVm);
        }
    }
}