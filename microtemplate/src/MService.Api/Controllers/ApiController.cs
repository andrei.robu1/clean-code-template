﻿namespace MService.Api.Controllers
{
    using Filters;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;

    [ApiController]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/mservice")]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    public abstract class ApiController : ControllerBase
    {
        private IMediator _mediator;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}