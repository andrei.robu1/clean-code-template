namespace MService.Api
{
    using Application;
    using Application.Interfaces;
    using Configuration;
    using Filters;
    using FluentValidation.AspNetCore;
    using Infrastructure;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Persistence;
    using Serilog;

    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => ApiConfig.CorsConfig(options, _configuration));

            services.AddHttpsRedirection(ApiConfig.HttpRedirectToHttpsConfig);
            services.AddSwaggerGen(SwaggerConfig.SwaggerGenConfig);
            services.AddApiVersioning(ApiConfig.ApiVersionConfig);

            services.AddRouting(options => options.LowercaseUrls = true);

            services.AddHealthChecks();

            services.AddApplication();

            services.AddPersistence();

            services.AddInfrastructure();

            services.AddScoped<ApiExceptionFilter>();

            services.AddControllers()
                .AddFluentValidation(s =>
                    s.RegisterValidatorsFromAssemblyContaining<IGuidGenerator>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger().UseSwaggerUI(SwaggerConfig.SwaggerUiConfig);
            }

            app.UseSecurityHeaders();

            app.UseSerilogRequestLogging();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                ApiConfig.HealthConfig(endpoints);
            });
        }
    }
}