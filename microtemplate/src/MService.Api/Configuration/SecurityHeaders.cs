﻿namespace MService.Api.Configuration
{
    using Microsoft.AspNetCore.Builder;

    public static class SecurityHeaders
    {
        public static void UseSecurityHeaders(this IApplicationBuilder app)
        {
            app.UseCors("CorsPolicy");

            app.UseCsp(options => options.FrameAncestors(s => s.Self()));
            app.UseXfo(o => o.SameOrigin());
            app.UseXContentTypeOptions();
            app.UseXXssProtection(config => config.Enabled());
        }
    }
}