﻿namespace MService.Api.Configuration
{
    using System;
    using System.IO;
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using Swashbuckle.AspNetCore.SwaggerUI;

    public static class SwaggerConfig
    {
        private static string Name => "MService";

        private static string Version => "v1";

        private static string Endpoint => $"/swagger/{Version}/swagger.json";

        /// <summary>
        ///     Gets route path for swagger UI e.q localhost:[port]/swagger.
        /// </summary>
        private static string UiEndpoint => "api/swagger";

        private static string ApiDescription => "Endpoint documentation for Carrier Rating API";

        public static void SwaggerUiConfig(SwaggerUIOptions config)
        {
            config.RoutePrefix = UiEndpoint;
            config.SwaggerEndpoint(Endpoint, Name);
        }

        public static void SwaggerGenConfig(SwaggerGenOptions config)
        {
            config.SwaggerDoc(
                Version,
                new OpenApiInfo { Version = Version, Title = Name, Description = ApiDescription });

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

            config.IncludeXmlComments(xmlPath);
        }
    }
}