﻿namespace MService.Api.Configuration
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.AspNetCore.Diagnostics.HealthChecks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.HttpsPolicy;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Versioning;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Diagnostics.HealthChecks;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public static class ApiConfig
    {
        public static void HealthConfig(IEndpointRouteBuilder endpoints)
        {
            endpoints.MapHealthChecks("/health", new HealthCheckOptions
            {
                ResultStatusCodes =
                {
                    [HealthStatus.Healthy] = StatusCodes.Status200OK, [HealthStatus.Degraded] = StatusCodes.Status500InternalServerError,
                    [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                },
                AllowCachingResponses = false,
                ResponseWriter = WriteHealthCheckResponse
            });
        }

        private static Task WriteHealthCheckResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";

            var json = new JObject(
                new JProperty("Overall status", result.Status.ToString()),
                new JProperty("Checks duration", result.TotalDuration.TotalSeconds.ToString("0:0.00")));

            return httpContext.Response.WriteAsync(json.ToString(Formatting.Indented));
        }

        public static void ApiVersionConfig(ApiVersioningOptions config)
        {
            config.DefaultApiVersion = new ApiVersion(1, 1);
            config.AssumeDefaultVersionWhenUnspecified = true;
            config.ReportApiVersions = true;
            config.ApiVersionReader = new HeaderApiVersionReader("X-Version");
        }

        public static void HstsConfig(HstsOptions config)
        {
            config.Preload = true;
            config.IncludeSubDomains = true;
            config.MaxAge = TimeSpan.FromHours(1);
        }

        public static void HttpRedirectToHttpsConfig(HttpsRedirectionOptions config)
        {
            config.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
        }

        public static void CorsConfig(CorsOptions options, IConfiguration config)
        {
            options.AddPolicy(
                "CorsPolicy",
                builder => builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins(config.GetSection("AllowedOrigin").Get<string[]>()));
        }
    }
}