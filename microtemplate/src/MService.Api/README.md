﻿# Presentation layer

This layer only include .NET core pipeline configuration, custom middleware and expose API endpoints.

Do's:
Configure routing, authentification/authorization, health checks, dependency injection, CORS, Swagger/OpenApi, MediatR.

Dont's:
Add business logic.
Add references to other layers other than Infrastructure, Common TBD. 
*Reference to the Persistance project is an exception in order to configure the layers dependencies.

