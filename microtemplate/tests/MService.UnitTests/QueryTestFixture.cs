﻿namespace MService.UnitTests
{
    using AutoMapper;
    using MService.Application.Mappings;

    public class QueryTestFixture
    {
        public QueryTestFixture()
        {
            var configurationProvider = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); });

            Mapper = configurationProvider.CreateMapper();
        }

        public IMapper Mapper { get; }
    }
}