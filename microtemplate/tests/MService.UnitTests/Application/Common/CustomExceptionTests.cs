﻿namespace MService.UnitTests.Application.Common
{
    using System.Collections.Generic;
    using FluentAssertions;
    using FluentValidation.Results;
    using MService.Application.Common.CustomExceptions;
    using MService.Common.Resources;
    using Xunit;

    public class CustomExceptionTests
    {
        [Fact]
        public void ValidationException_Should_Build_Failure_Dictionary()
        {
            //arrange
            const string expectedKey = "Key";
            const string expectedDescription = "ValidationMessage";

            var failedValidationsExpected = new List<ValidationFailure>
            {
                new ValidationFailure(expectedKey, expectedDescription)
            };

            //act
            var validationException = new ValidationException(failedValidationsExpected);

            //assert
            validationException.Failures[expectedKey].Should().BeEquivalentTo(failedValidationsExpected[0].ErrorMessage);
        }

        [Fact]
        public void BadRequestException_Should_Have_A_Default_Bad_Request_Message()
        {
            var badRequestException = new BadRequestException();

            badRequestException.UiMessage.Should().BeEquivalentTo(ExceptionMessages.BadRequest);
        }

        [Fact]
        public void BadRequestException_Should_Have_A_Custom_Bad_Request_Message()
        {
            const string expectedCustomMessage = "Custom error message.";
            var badRequestException = new BadRequestException(expectedCustomMessage);

            badRequestException.UiMessage.Should().BeEquivalentTo(expectedCustomMessage);
        }

        [Fact]
        public void ForbiddenException_Should_Have_A_Default_Forbidden_Message()
        {
            var forbiddenException = new ForbiddenException();

            forbiddenException.UiMessage.Should().BeEquivalentTo(ExceptionMessages.Forbidden);
        }

        [Fact]
        public void ForbiddenException_Should_Have_A_Custom_Forbidden_Message_With_The_Resource_Name()
        {
            const string expectedCustomMessage = "ForbiddenException is forbidden.";
            var forbiddenException = new ForbiddenException(nameof(ForbiddenException));

            forbiddenException.UiMessage.Should().BeEquivalentTo(expectedCustomMessage);
        }


        [Fact]
        public void NotFoundException_Should_Have_A_Default_NotFound_Message()
        {
            var notFoundException = new NotFoundException();

            notFoundException.UiMessage.Should().BeEquivalentTo(ExceptionMessages.NotFound);
        }

        [Fact]
        public void NotFoundException_Should_Have_A_Custom_NotFound_Message_With_The_Resource_Name()
        {
            const string expectedCustomMessage = "NotFoundException not found.";
            var notFoundException = new NotFoundException(nameof(NotFoundException));

            notFoundException.UiMessage.Should().BeEquivalentTo(expectedCustomMessage);
        }
    }
}