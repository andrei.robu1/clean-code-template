﻿namespace MService.UnitTests.Application.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FluentAssertions;
    using MediatR;
    using MService.Application.Users.Queries.GetUser;
    using Xunit;

    public class HandlerRegistrationTests
    {
        [Fact]
        public void AllRequests_ShouldHaveMatchingHandler()
        {
            var requestTypes = typeof(GetUserQuery).Assembly.GetTypes()
                .Where(IsRequest)
                .ToList();

            var handlerTypes = typeof(GetUserQuery).Assembly.GetTypes()
                .Where(IsIRequestHandler)
                .ToList();

            foreach (var requestType in requestTypes) ShouldContainHandlerForRequest(handlerTypes, requestType);
        }

        private static void ShouldContainHandlerForRequest(IEnumerable<Type> handlerTypes, Type requestType)
        {
            handlerTypes.Should().ContainSingle(handlerType => IsHandlerForRequest(handlerType, requestType),
                $"Handler for type {requestType} expected. Each request should have a handler class associated!");
        }

        private static bool IsRequest(Type type)
        {
            return typeof(IBaseRequest).IsAssignableFrom(type);
        }

        private static bool IsIRequestHandler(Type type)
        {
            return type.GetInterfaces().Any(interfaceType =>
                interfaceType.IsGenericType && interfaceType.GetGenericTypeDefinition() == typeof(IRequestHandler<,>));
        }

        private static bool IsHandlerForRequest(Type handlerType, Type requestType)
        {
            return handlerType.GetInterfaces().Any(i => i.GenericTypeArguments.Any(ta => ta == requestType));
        }
    }
}